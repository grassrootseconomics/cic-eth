class LoopDone(Exception):
    """Exception raised when a syncing is complete.
    """
    pass
