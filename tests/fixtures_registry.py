# third-party imports
import pytest
import os
import json

# local imports
from cic_registry import CICRegistry
from cic_registry.contract import Contract
from cic_registry.error import ChainExistsError

